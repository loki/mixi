# Copyright 2017,2018 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A small daemon managing kernel hotplug events, similarly to udevd."
HOMEPAGE="http://skarnet.org/software/${PN}/"
DOWNLOADS="http://skarnet.org/software/${PN}/${PNV}.tar.gz"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    nsss [[ description = [ Use the nsss library for user database lookups ] ]]
    static
"

DEPENDENCIES="
    build+run:
        dev-libs/skalibs[>=2.7.0.0]
        nsss? ( sys-utils/nsss )
"

BUGS_TO="mixi@exherbo.org"

# CROSS_COMPILE: because configure only expects executables to be prefixed if build != host
# REALCC: because the makefile prefixes configure's CC with $(CROSS_COMPILE)
DEFAULT_SRC_COMPILE_PARAMS=(
    REALCC=${CC}
    CROSS_COMPILE=$(exhost --target)-
)

src_configure()
{
    local args=(
        --enable-shared
        --with-lib=/usr/$(exhost --target)/lib
        --with-dynlib=/usr/$(exhost --target)/lib
        $(option_enable nsss)
        $(option_enable static allstatic)
    )

    [[ $(exhost --target) == *-gnu* ]] || \
        args+=( $(option_enable static static-libc) )

    econf "${args[@]}"
}

src_install()
{
    default

    dodoc -r examples

    insinto /usr/share/doc/${PNVR}/html
    doins doc/*
}

